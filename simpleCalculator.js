
function handleClick(operator) {
    let num1Input = document.querySelector('#num1Input');
    let num2Input = document.querySelector('#num2Input');
    let resultContainer = document.querySelector('#resultContainer');

    let num1 = parseInt(num1Input.value);
    let num2 = parseInt(num2Input.value);

    let result = 0

    switch(operator) {
        case '+':
            result = num1 + num2;
            resultContainer.textContent = result;
            break;
        case '-':
            result = num1 - num2;
            resultContainer.textContent = result;
            break;
        case '*':
            result = num1 * num2;
            resultContainer.textContent = result;
            break;
        case '/':
            result = num1 / num2;
            resultContainer.textContent = result;
            break;
        case 'C':
            num1Input.value = '';
            num2Input.value = '';  
            resultContainer.textContent = "result";
            break;
    }
}
//see ülemine on kompaktsem ja ilusam kood.

/*function handlePlusClick() {
    let num1Input = document.querySelector('#num1Input');
    let num2Input = document.querySelector('#num2Input');
    let resultContainer = document.querySelector('#resultContainer');

    let num1 = parseInt(num1Input.value);
    let num2 = parseInt(num2Input.value);
    let result = num1 + num2;

    resultContainer.textContent = result;
}
//küsin esimese ja teise elemendi väätused ja küsin juurdepääsu result reale, siis ütlen, et võta esimene ja teine number, liida kokku ja näita result real

function handleMinusClick() {
    let num1Input = document.querySelector('#num1Input');
    let num2Input = document.querySelector('#num2Input');
    let resultContainer = document.querySelector('#resultContainer');

    let num1 = parseInt(num1Input.value);
    let num2 = parseInt(num2Input.value);
    let result = num1 - num2;

    resultContainer.textContent = result;
}
function handleMultClick() {
    let num1Input = document.querySelector('#num1Input');
    let num2Input = document.querySelector('#num2Input');
    let resultContainer = document.querySelector('#resultContainer');

    let num1 = parseInt(num1Input.value);
    let num2 = parseInt(num2Input.value);
    let result = num1 * num2;

    resultContainer.textContent = result;
}
function handleDivClick() {
    let num1Input = document.querySelector('#num1Input');
    let num2Input = document.querySelector('#num2Input');
    let resultContainer = document.querySelector('#resultContainer');

    let num1 = parseInt(num1Input.value);
    let num2 = parseInt(num2Input.value);
    let result = num1 / num2;

    resultContainer.textContent = result;
}
function handleClearClick() {
    let num1Input = document.querySelector('#num1Input');
    let num2Input = document.querySelector('#num2Input');
    let resultContainer = document.querySelector('#resultContainer');

    num1Input.value = '';
    num2Input.value = '';
    resultContainer.textContent = 'Result';
}*/
